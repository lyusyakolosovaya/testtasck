import React from 'react';
import './timerStyle.scss';

export default class Timer extends React.Component {
  state = {
    hours: 0,
    minutes: 5,
    seconds: 0,
  };

  componentDidMount() {
    setInterval(this.startTimer, 1000);
  }

  startTimer = () => {
    if (this.state.seconds === 0) {
      this.setState((prevState) => ({
        minutes: prevState.minutes - 1,
        seconds: 59,
      }));
    } else {
      this.setState((prevState) => ({
        seconds: prevState.seconds - 1,
      }));
    }

    if (this.state.seconds === 0 && this.state.minutes === 0) {
      this.setState({
        minutes: 5,
        seconds: 0,
      });
    }
  };

  render() {
    return (
      <div className="container" id="timer">
        <h2 className="title">Timer</h2>
        <div className="timer">
          <span className="minute">{this.state.hours <= 9 ? `0${this.state.hours}` : this.state.hours}h&#160;</span>
          <span className="minute">{this.state.minutes <= 9 ? `0${this.state.minutes}` : this.state.minutes}m&#160;</span>
          <span className="seconds">{this.state.seconds <= 9 ? `0${this.state.seconds}` : this.state.seconds}s</span>
        </div>
        <a
          href="#form"
          className="button"
        >
          Get
        </a>
      </div>
    );
  }
}
