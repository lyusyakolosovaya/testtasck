import React from 'react';
import './styles.scss';
import '../../reset.css';

export default function Menu() {
  return (
    <ul className="menu">
      <li className="menu__item"><a href="#top">Top</a></li>
      <li className="menu__item"><a href="#timer">Timer</a></li>
      <li className="menu__item"><a href="#form">Form</a></li>
      <li className="menu__item"><a href="#slider">Carousel</a></li>
      <li className="menu__item"><a href="#footer">Footer</a></li>
    </ul>
  );
}
