import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { openModal, setUser, setNumber } from '../action';

import './form.scss';


class Form extends React.Component {
  validateNumber(e) {
    const itemArray = e.target.value.split('').filter((item) => !isNaN(item));
    const deleteItem = itemArray.length > 11 ? itemArray.length - 11 : 0;

    itemArray.splice(10, deleteItem);
    e.target.value = itemArray.join('');
    this.props.setNumber(e.target.value);
  }

  setUserName(e) {
    this.props.setUser(e.target.value);
  }

  validateForm = (e) => {
    this.props.openModal(true);
    e.preventDefault();
  };

  render() {
    return (
      <div className="container" id="form">
        <h2 className="title">Form</h2>
        <form className="form">
          <h2 className="form__title">User information</h2>
          <label className="form__field">
            <span>User name</span>
            <input
              type="text"
              className="form__input"
              onBlur={(e) => this.setUserName(e)}
              required
            />
          </label>
          <label className="form__field">
            <span>Tel.</span>
            <input
              onChange={(e) => this.validateNumber(e)}
              type="tel"
              className="form__input"
              required
            />
          </label>
          <button
            className="form__button button"
            onClick={(e) => this.validateForm(e)}
            type="button"
          >
          Send
          </button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  openModal,
  setUser,
  setNumber,
};

Form.propTypes = {
  openModal: PropTypes.func.isRequired,
  setUser: PropTypes.func.isRequired,
  setNumber: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(Form);
