export const actionType = {
  openModal: 'OPEN_MODAL',
  setUser: 'SET_USER',
  setNumber: 'SET_NUMBER',
};

export function openModal(payload) {
  return {
    type: actionType.openModal,
    payload,
  };
}

export function setUser(payload) {
  return {
    type: actionType.setUser,
    payload,
  };
}

export function setNumber(payload) {
  return {
    type: actionType.setNumber,
    payload,
  };
}
