import {actionType} from './action';

const stateInit = {
  showModal: false,
  user: '',
  number: '',
};

export default function reducer(state = stateInit, action) {
  switch (action.type) {
    case actionType.openModal:
      return {
        ...state,
        showModal: action.payload,
      };

    case actionType.setUser:
      return {
        ...state,
      user: action.payload,
      };

    case actionType.setNumber:
      return {
        ...state,
      number: action.payload,
      };

    default:
      return state;
  }
}
