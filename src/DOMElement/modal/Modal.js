import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { openModal } from '../action';

import './modal.scss';

class Modal extends React.PureComponent {
  render() {
    const modalContainerClassList = classNames('modal-container', {
      'modal-container--open': this.props.showModal,
    });
    const modalClassList = classNames('modal', {
      'modal--open': this.props.showModal,
    });

    return (
      <div className={modalContainerClassList}>
        <div className={modalClassList}>
          <h3 className="modal__title">Information</h3>
          <p className="modal__text">{
            this.props.user && this.props.number
              ? (<>User: {this.props.user}<br /> Number: {this.props.number}</>)
              : <>Incorrect data</>

          }
          </p>
          <button
            className="button"
            onClick={() => this.props.openModal(false)}
            type="button"
          >
            Ok
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  showModal: state.showModal,
  user: state.user,
  number: state.number,
});

const mapDispatchToProps = {
  openModal,
};

Modal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  user: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired,
  openModal: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
