import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './slider.scss';

export default class Slider extends Component {
  slides = [];

  activeIndex;

  box = React.createRef();

  sliderWrap = React.createRef();

  static prevDirection = 1;

  static nextDirection = -1;

  componentDidMount() {
    this.activeIndex = this.slides.length - 1;
    this.setInitialStyles();
    this.changeSizes();
    this.carucel();
  }

  setInitialStyles() {
    if (this.sliderWrap) {
      this.slideWidth = this.box.current.clientWidth / this.config.visibleSlides;
      this.sliderWrap.current.style.width = `${this.slides.length * this.slideWidth}px`;
      this.sliderWrap.current.style.height = `${this.box.current.clientHeight}px`;

      this.slides.forEach((slide) => {
        slide.style.width = `${this.slideWidth}px`;
        slide.style.height = `${this.box.clientHeight}px`;
      });

      this.sliderWrap.current.style.transform = `translateX(${-this.slideWidth}px)`;
      this.setLeftPosition(Slider.nextDirection);
    }
  }

  setLeftPosition(direction) {
    let quantity = 0;
    let index = this.activeIndex;

    while (quantity < this.slides.length) {
      this.slides[index].style.left = `${quantity * this.slideWidth}px`;
      index = this.calcNewActiveIndex(index, direction);
      quantity++;
    }
  }

  changeSizes() {
    const middleItem = Math.ceil(this.props.visibleSlides / 2);
    let middleIndex;

    if (this.activeIndex + middleItem < this.slides.length) {
      middleIndex = this.activeIndex + middleItem;
    } else {
      middleIndex = middleItem - (this.slides.length - this.activeIndex);
    }
    this.slides[middleIndex].style.transform = 'scale(1)';
    for (let i = middleItem - 1; i > 0; i--) {
      const coef = 1 - (0.5 / middleItem * i);

      this.slides[this.calcNewActiveIndex(middleIndex, Slider.nextDirection * i)].style.transform = `scale(${coef})`;
      this.slides[this.calcNewActiveIndex(middleIndex, Slider.prevDirection * i)].style.transform = `scale(${coef})`;
    }
  }

  calcNewActiveIndex(activeIndex, direction) {
    let index;

    if (direction / Math.abs(direction) === Slider.nextDirection) {
      index = activeIndex >= this.slides.length + direction
        ? activeIndex - this.slides.length - direction
        : activeIndex - direction;
    } else {
      index = activeIndex < direction
        ? this.slides.length - direction + activeIndex
        : activeIndex - direction;
    }

    return index;
  }

  carucel() {
    let x0;
    const onMouseMove = (event) => {
      const { x } = event;
      const translate = this.calcTranslate(x, x0);

      if (this.sliderWrap.current) {
        this.sliderWrap.current.style.transform = `translateX(${-this.slideWidth
      + translate}px)`;
      }
    };
    const onMouseUp = (event) => {
      const { x } = event;
      let direction = this.calcTranslate(x, x0);

      direction /= Math.abs(direction);
      this.move(direction);
      window.removeEventListener('mousemove', onMouseMove);
    };
    const onMouseDown = (event) => {
      if (this.preventSliding) {
        return;
      }
      x0 = event.x;
      window.addEventListener('mousemove', onMouseMove);
      window.addEventListener('mouseup', onMouseUp, { once: true });
    };

    this.box.current.addEventListener('mousedown', onMouseDown);
  }

  calcTranslate(x, x0) {
    const translate = x - x0;

    if (translate > this.slideWidth) {
      return this.slideWidth - 1;
    } else {
      return translate < -this.slideWidth
        ? -this.slideWidth + 1
        : translate;
    }
  }

  move(direction) {
    if (this.sliderWrap.current) {
      if (this.preventSliding) {
        return;
      }
      const initTranslate = +this.sliderWrap.current.style.transform.split(
        'translateX(',
      )[1].split('px)')[0] || 0;
      const newTranslate = this.slideWidth * (direction - 1);

      this.preventSliding = initTranslate !== newTranslate;
      this.activeIndex = this.calcNewActiveIndex(this.activeIndex, direction);
      this.sliderWrap.current.style.transform = `translateX(${newTranslate}px)`;
      this.sliderWrap.current.style.transition = `${this.config.transition}ms ease-in-out`;
      this.slides.forEach((slide) => {
        slide.style.transition = `transform ${this.config.transition}ms ease-in-out`;
      });
      this.changeSizes();
      if (this.preventSliding) {
        this.changeSlidesPosition();
      }
    }
  }

  changeSlidesPosition() {
    this.sliderWrap.current.addEventListener('transitionend', () => {
      this.setLeftPosition(-1);
      if (this.sliderWrap.current) {
        this.sliderWrap.current.style.transform = `translateX(${-this.slideWidth}px)`;
        this.sliderWrap.current.style.transition = 'none';
      }

      this.preventSliding = false;
    }, { once: true });
  }

  get config() {
    return {
      visibleSlides: this.props.visibleSlides,
      transition: this.props.transition,
    };
  }


  render() {
    return (
      <div className="container" id="slider">
        <h2 className="title">
          Slider
        </h2>
        <div className="slider-container">
          <button
            className="slider__btn slider__btn--left"
            type="button"
            onClick={() => this.move(Slider.nextDirection)}
          >&#60;
          </button>
          <div className="slider" ref={this.box}>
            <div className="wrapper" ref={this.sliderWrap}>
              {this.props.children.map((child, index) => React.cloneElement(
                child,
                {
                  ref: (el) => this.slides.push(el),
                  key: index,
                },
              ))}
            </div>
          </div>
          <button className="slider__btn slider__btn--right" type="button" onClick={() => this.move(Slider.prevDirection)}>&#62;</button>
        </div>
      </div>
    );
  }
}

Slider.propTypes = {
  visibleSlides: PropTypes.number.isRequired,
  transition: PropTypes.number.isRequired,
  children: PropTypes.arrayOf(PropTypes.object).isRequired,
};
