import React from 'react';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';

import * as serviceWorker from './serviceWorker';
import reducer from './DOMElement/reducer';

import App from './App';

import './index.css';

const store = createStore(reducer, composeWithDevTools());
const app = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
