import React from 'react';
import Menu from './DOMElement/Menu/Menu';
import Slider from './DOMElement/carusel/Carusel';
import Timer from './DOMElement/Timer/Timer';
import Form from './DOMElement/Form/Form';

import './App.css';
import Modal from './DOMElement/modal/Modal';

function App() {
  return (
    <>
      <header id="top" className="App-header">
        <Menu />
      </header>
      <main className="App">
        <Timer />
        <Form />
        <Slider visibleSlides={5} transition={300}>
          <div className="slide">1</div>
          <div className="slide">2</div>
          <div className="slide">3</div>
          <div className="slide">4</div>
          <div className="slide">5</div>
          <div className="slide">6</div>
          <div className="slide">7</div>
          <div className="slide">8</div>
          <div className="slide">9</div>
          <div className="slide">10</div>
        </Slider>
      </main>
      <footer className="footer" id="footer">
        <ul className="footer__list">
          <li className="footer__list-item">
            <a href="mailto: lyusyakolsovaya@gmail.com">
            Email
            </a>
          </li>
          <li className="footer__list-item">
            <a href="tel: +380673020508">
            My number
            </a>
          </li>
          <li className="footer__list-item">
            <a href="skype: live:d60a685e17185325">
            My Skype
            </a>
          </li>
        </ul>
        ® Lucy Kolosova 2019.
      </footer>
      <Modal />
    </>
  );
}

export default App;
